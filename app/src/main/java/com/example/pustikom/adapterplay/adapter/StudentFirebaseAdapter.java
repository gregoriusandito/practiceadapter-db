package com.example.pustikom.adapterplay.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.example.pustikom.adapterplay.R;
import com.example.pustikom.adapterplay.user.Student;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

/**
 * Created by Gregorius Andito on 12/7/2016.
 */

public class StudentFirebaseAdapter extends FirebaseListAdapter<Student> {

    /**
     * @param activity    The activity containing the ListView
     * @param modelClass  Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout This is the layout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of modelClass.
     * @param ref         The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                    combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public StudentFirebaseAdapter(Activity activity, Class<Student> modelClass, int modelLayout, Query ref) {
        super(activity, modelClass, modelLayout, ref);
    }

    @Override
    protected void populateView(View view, Student student, int position) {
        TextView idView = (TextView) view.findViewById(R.id.student_id);
        TextView noregView = (TextView) view.findViewById(R.id.student_noreg);
        TextView nameView = (TextView) view.findViewById(R.id.student_name);
        TextView genderView = (TextView) view.findViewById(R.id.student_gender);
        TextView mailView = (TextView) view.findViewById(R.id.student_email);
        TextView phoneView = (TextView) view.findViewById(R.id.student_phone);
        idView.setText(position+1 + "");
        noregView.setText(student.getNoreg());
        nameView.setText(student.getName());
        switch (student.getGender()){
            case 0:
                genderView.setText("L");
                break;
            case 1:
                genderView.setText("P");
                break;
        }
        mailView.setText(student.getMail());
        phoneView.setText(student.getPhone());
    }
}
